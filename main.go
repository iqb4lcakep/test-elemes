// Muhammad Iqbal Rofikurrahman - 2021
package main

import (
	"fmt"
	"net/http"
	"test_elemes/application"
	"test_elemes/controller"
	"test_elemes/helpers"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	fmt.Println("Starting server test")
	godotenv.Load()

	// START CONNECTION
	app_context := application.AppContext{
		Db: helpers.ConnectMysql(),
	}
	ref_app_context := &app_context

	// START HTTP SERVICE
	r := gin.Default()

	// ALLOW CORS
	r.Use(func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Allow-Headers", "*")
		c.Header("Content-Type", "application/json")
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusOK)
			return
		}
		c.Next()
	})

	// r.GET("/version", func(c *gin.Context) {
	// 	c.String(http.StatusOK, ref_app_context.GetVerison())
	// })

	auth_ctrl := controller.NewAuthController(ref_app_context)
	user_ctrl := controller.NewUsercontroller(ref_app_context)
	kursus_ctrl := controller.NewKursusController(ref_app_context)
	stats_ctrl := controller.NewStatsController(ref_app_context)

	r.POST("/login", auth_ctrl.Login)
	r.POST("/upload", kursus_ctrl.UploadImage)

	// ADMIN PATH
	rAdmin := r.Group("/admin", controller.AdminMiddleware(ref_app_context))
	rAdmin.POST("/dashboard", stats_ctrl.Dashboard)

	rUserAdmin := rAdmin.Group("/user")
	r.POST("/createUser", user_ctrl.CreateUser)
	rUserAdmin.POST("/updateUser", user_ctrl.UpdateUser)
	rUserAdmin.POST("/getUser", user_ctrl.GetUser)
	rUserAdmin.POST("/users", user_ctrl.Users)
	rUserAdmin.POST("/deleteUser", user_ctrl.DeleteUser)

	rKursusAdmin := rAdmin.Group("/kursus")
	rKursusAdmin.POST("/createKursus", kursus_ctrl.CreateKursus)
	rKursusAdmin.POST("/updateKursus", kursus_ctrl.UpdateKursus)
	rKursusAdmin.POST("/deleteKursus", kursus_ctrl.DeleteKursus)

	// USER PATH
	rUser := r.Group("/user", controller.AuthMiddleware(ref_app_context))
	rKursusUser := rUser.Group("/kursus")
	rKursusUser.POST("/getKursus", kursus_ctrl.GetKursus)
	rKursusUser.POST("/kursus", kursus_ctrl.Kursus)

	r.MaxMultipartMemory = 8 << 20
	r.Use(static.Serve("/", static.LocalFile("./public", true)))

	r.Run(":" + helpers.EnvVal("SERVER_PORT", "8080"))
}
