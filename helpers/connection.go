package helpers

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func ConnectMysql() *gorm.DB {
	con_string := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		EnvVal("DB_USER", "root"),
		EnvVal("DB_PASS", ""),
		EnvVal("DB_HOST", "localhost"),
		EnvVal("DB_PORT", "3306"),
		EnvVal("DB_NAME", "test_elemes"),
	)

	db, err := gorm.Open("mysql", con_string)
	if err != nil {
		panic("DB CONNECTION ERROR : " + err.Error())
	}
	_ = db.DB().Ping()
	db.SingularTable(true)
	db.LogMode(true)
	return db
}
