package helpers

import "os"

func EnvVal(key, fallback string) string {
	val, exists := os.LookupEnv(key)
	if !exists {
		val = fallback
	}
	return val
}
