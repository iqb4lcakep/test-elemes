package controller

import (
	"fmt"
	"net/http"
	"strings"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"
	"test_elemes/helpers"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthMiddleware(ctx *application.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		payload, token, err := TokenValid(c.Request)
		c.Set("auth", payload)

		// check session
		session := domaindata.Session{}
		q := ctx.Db.Unscoped().Scopes(application.Active).Where("session = ?", token).First(&session)
		if q.RecordNotFound() || err != nil {
			ctx.Error(c, http.StatusUnauthorized, "invalid token", application.ErrAuth)
			c.Abort()
			return
		}
		c.Next()
	}
}

func AdminMiddleware(ctx *application.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get("Authorization")
		if token != "adminonly" {
			ctx.Error(c, http.StatusUnauthorized, "invalid token", application.ErrAuth)
			c.Abort()
			return
		}
		c.Next()
	}
}

func ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	//normally Authorization the_token_xxx
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func VerifyToken(r *http.Request) (*jwt.Token, string, error) {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(helpers.EnvVal("JWT_KEY", "test_elemes")), nil
	})
	if err != nil {
		return nil, "", err
	}
	return token, tokenString, nil
}

func TokenValid(r *http.Request) (domaindata.AuthJwtPayload, string, error) {
	token, token_str, err := VerifyToken(r)
	payload := domaindata.AuthJwtPayload{}
	if err != nil {
		return payload, token_str, err
	}
	res, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		return payload, token_str, err
	}

	payload.UserID = int32(res["user_id"].(float64))
	return payload, token_str, nil
}
