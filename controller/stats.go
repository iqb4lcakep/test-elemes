package controller

import (
	"net/http"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"

	"github.com/gin-gonic/gin"
)

type StatsController struct {
	Ctx *application.AppContext
}

func NewStatsController(Ctx *application.AppContext) *StatsController {
	return &StatsController{
		Ctx,
	}
}

func (ctrl *StatsController) Dashboard(c *gin.Context) {
	query := `SELECT (
		SELECT COUNT(user_id) FROM USER u WHERE u.STATUS=1) AS total_user,(
		SELECT COUNT(kursus_id) FROM kursus k WHERE k.STATUS=1) AS total_kursus,(
		SELECT COUNT(kursus_id) FROM kursus k WHERE k.STATUS=1 AND k.kursus_harga=0.0) AS total_kursus_free`

	var dashboard_stats domaindata.DashboardStats
	ctrl.Ctx.Db.Raw(query).Scan(&dashboard_stats)
	if err := ctrl.Ctx.Db.Error; err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
	}
	ctrl.Ctx.Success(c, "success", dashboard_stats)
}
