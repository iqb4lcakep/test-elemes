package controller

import (
	"net/http"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"

	"github.com/gin-gonic/gin"
)

type KategoriController struct {
	Ctx *application.AppContext
}

func NewKategoriController(Ctx *application.AppContext) *KategoriController {
	return &KategoriController{
		Ctx,
	}
}

func (ctrl *KategoriController) GetKategori(c *gin.Context) {
	var req domaindata.GetKategoriParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kategori := domaindata.Kategori{
		KategoriID: req.KategoriID,
	}
	if ctrl.Ctx.Db.Unscoped().First(&kategori).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "kategori not found", application.ErrInput)
		return
	}

	ctrl.Ctx.Success(c, "succes", &kategori)
}
