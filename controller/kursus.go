package controller

import (
	"net/http"
	"path/filepath"
	"strings"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"
	"test_elemes/helpers"
	"time"

	"github.com/gin-gonic/gin"
)

type KursusController struct {
	Ctx *application.AppContext
}

func NewKursusController(Ctx *application.AppContext) *KursusController {
	return &KursusController{
		Ctx,
	}
}

func (ctrl *KursusController) CreateKursus(c *gin.Context) {
	var req domaindata.CreateKursusParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kursus := domaindata.Kursus{
		KursusNama:      req.KursusNama,
		KategoriID:      req.KategoriID,
		KursusHarga:     req.KursusHarga,
		KursusGambarURL: req.KursusGambarURL,
	}

	err = ctrl.Ctx.Db.Create(&kursus).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "succes save", &kursus)
}

func (ctrl *KursusController) UpdateKursus(c *gin.Context) {
	var req domaindata.UpdateKursusParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kursus := domaindata.Kursus{
		KursusID: req.KursusID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&kursus).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "kursus not found", application.ErrInput)
		return
	}

	// produk mapping manual
	if req.KursusNama != nil {
		kursus.KursusNama = *req.KursusNama
	}
	if req.KategoriID != nil {
		kursus.KategoriID = *req.KategoriID
	}
	if req.KursusHarga != nil {
		kursus.KursusHarga = *req.KursusHarga
	}
	if req.KursusGambarURL != nil {
		kursus.KursusGambarURL = *req.KursusGambarURL
	}

	if err := ctrl.Ctx.Db.Unscoped().Save(&kursus).Error; err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}
	ctrl.Ctx.Success(c, "succes update", &kursus)
}

func (ctrl *KursusController) GetKursus(c *gin.Context) {
	var req domaindata.GetKursusParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kursus := domaindata.Kursus{
		KursusID: req.KursusID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&kursus).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "kursus not found", application.ErrInput)
		return
	}

	ctrl.Ctx.Success(c, "succes update", &kursus)
}

func (ctrl *KursusController) Kursus(c *gin.Context) {
	var req domaindata.ListGlobalParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kursus := []domaindata.Kursus{}
	err = ctrl.Ctx.Db.Unscoped().
		Scopes(application.Active).
		Scopes(application.Pagination(req.Limit, req.Offset)).
		Scopes(application.Sort(req.OrderColumn, req.OrderType)).
		Where("kursus_nama LIKE ?", "%"+strings.TrimSpace(req.Search)+"%").
		Find(&kursus).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "success", &kursus)
}

func (ctrl *KursusController) DeleteKursus(c *gin.Context) {
	var req domaindata.GetKursusParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	kursus := domaindata.Kursus{
		KursusID: req.KursusID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&kursus).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "kursus not found", application.ErrInput)
		return
	}

	kursus.Status = 0
	kursus.DeletedAt = time.Now()

	if err := ctrl.Ctx.Db.Unscoped().Save(&kursus).Error; err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}
	ctrl.Ctx.Success(c, "succes delete", gin.H{})
}

func (ctrl *KursusController) UploadImage(c *gin.Context) {
	file, err := c.FormFile("file")
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	filename := filepath.Base(file.Filename)
	if err := c.SaveUploadedFile(file, "./public/"+filename); err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "succes upload", gin.H{
		"url": "http://localhost:" + helpers.EnvVal("SERVER_PORT", "8080") + "/" + file.Filename,
	})
}
