package controller

import (
	"net/http"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"

	"github.com/gin-gonic/gin"
)

type AuthController struct {
	Ctx *application.AppContext
}

func NewAuthController(Ctx *application.AppContext) *AuthController {
	return &AuthController{
		Ctx,
	}
}

func (ctrl *AuthController) Login(c *gin.Context) {
	var req domaindata.LoginParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	var user domaindata.User
	// email check
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).Where(&domaindata.User{
		UserEmail: req.UserEmail,
	}).First(&user).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, "Email not found", application.ErrServer)
		return
	}

	// password verify
	if !user.VerifyPassword(req.UserPassword) {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, "Login failed", application.ErrInput)
		return
	}

	payload := &domaindata.AuthJwtPayload{
		UserID: user.UserID,
	}

	// storing session
	session_token, err := payload.CreateToken()
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	err = ctrl.Ctx.Db.Table("session").Scopes(application.Active).Where("user_id = ?", user.UserID).Update("status", 0).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	err = ctrl.Ctx.Db.Create(&domaindata.Session{
		UserID:  user.UserID,
		Session: session_token,
		Status:  1,
	}).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "login success", gin.H{
		"session": session_token,
	})
}
