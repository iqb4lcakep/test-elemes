package controller

import (
	"net/http"
	"strings"
	"test_elemes/application"
	domaindata "test_elemes/domain_data"
	"time"

	"github.com/gin-gonic/gin"
)

type UserController struct {
	Ctx *application.AppContext
}

func NewUsercontroller(Ctx *application.AppContext) *UserController {
	return &UserController{
		Ctx,
	}
}

func (ctrl *UserController) CreateUser(c *gin.Context) {
	var req domaindata.CreateUserParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	user := domaindata.User{
		UserNama:  req.UserNama,
		UserEmail: req.UserEmail,
	}
	if ok, err := user.GenerateHashPassword(req.UserPassword); !ok || err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	err = ctrl.Ctx.Db.Create(&user).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "succes save", &user)
}

func (ctrl *UserController) UpdateUser(c *gin.Context) {
	var req domaindata.UpdateUserParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	user := domaindata.User{
		UserID: req.UserID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&user).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "user not found", application.ErrInput)
		return
	}
	// user mapping manual
	if req.UserNama != nil {
		user.UserNama = *req.UserNama
	}
	if req.UserEmail != nil {
		user.UserEmail = *req.UserEmail
	}
	if req.UserPassword != nil {
		if _, err := user.GenerateHashPassword(*req.UserPassword); err != nil {
			ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
			return
		}
	}

	if err := ctrl.Ctx.Db.Unscoped().Save(&user).Error; err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}
	ctrl.Ctx.Success(c, "succes update", &user)
}

func (ctrl *UserController) GetUser(c *gin.Context) {
	var req domaindata.GetUserParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	user := domaindata.User{
		UserID: req.UserID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&user).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "user not found", application.ErrInput)
		return
	}

	ctrl.Ctx.Success(c, "succes", &user)
}

func (ctrl *UserController) Users(c *gin.Context) {
	var req domaindata.ListGlobalParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	users := []domaindata.User{}
	err = ctrl.Ctx.Db.Unscoped().
		Scopes(application.Active).
		Scopes(application.Pagination(req.Limit, req.Offset)).
		Where("user_nama LIKE ?", "%"+strings.TrimSpace(req.Search)+"%").
		Find(&users).Error
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}

	ctrl.Ctx.Success(c, "succes", &users)
}

func (ctrl *UserController) DeleteUser(c *gin.Context) {
	var req domaindata.GetUserParameter
	err := c.ShouldBind(&req)
	if err != nil {
		ctrl.Ctx.Error(c, http.StatusBadRequest, err.Error(), application.ErrInput)
		return
	}

	user := domaindata.User{
		UserID: req.UserID,
	}
	if ctrl.Ctx.Db.Unscoped().Scopes(application.Active).First(&user).RecordNotFound() {
		ctrl.Ctx.Error(c, http.StatusBadRequest, "user not found", application.ErrInput)
		return
	}

	user.Status = 0
	user.DeletedAt = time.Now()

	if err := ctrl.Ctx.Db.Unscoped().Save(&user).Error; err != nil {
		ctrl.Ctx.Error(c, http.StatusInternalServerError, err.Error(), application.ErrServer)
		return
	}
	ctrl.Ctx.Success(c, "succes delete", gin.H{})

}
