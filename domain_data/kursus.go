package domaindata

import (
	"time"
)

type Kursus struct {
	KursusID        int32     `gorm:"primary_key;AUTO_INCREMENT;column:kursus_id;type:int;" json:"kursus_id"`
	KursusNama      string    `gorm:"column:kursus_nama;type:varchar;size:100;" json:"kursus_nama"`
	KategoriID      int32     `gorm:"column:kategori_id;type:int;" json:"kategori_id"`
	KursusHarga     float64   `gorm:"column:kursus_harga;type:decimal;" json:"kursus_harga"`
	Status          int32     `gorm:"column:status;type:tinyint;default:1;" json:"status"`
	KursusGambarURL string    `gorm:"column:kursus_gambar_url;type:text;size:65535;" json:"kursus_gambar_url"`
	CreatedAt       time.Time `gorm:"column:created_at;type:timestamp;" json:"created_at"`
	UpdatedAt       time.Time `gorm:"column:updated_at;type:timestamp;" json:"updated_at"`
	DeletedAt       time.Time `gorm:"column:deleted_at;type:timestamp;" json:"deleted_at"`
}

type KursusUser struct {
	ID       int32 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:int;" json:"id"`
	UserID   int32 `gorm:"column:user_id;type:int;" json:"user_id"`
	KursusID int32 `gorm:"column:kursus_id;type:int;" json:"kursus_id"`
	Status   int32 `gorm:"column:status;type:tinyint;default:1;" json:"status"`
}

type GetKursusParameter struct {
	KursusID int32 `form:"kursus_id" json:"kursus_id" binding:"required"`
}

type CreateKursusParameter struct {
	KursusNama      string  `form:"kursus_nama" json:"kursus_nama" binding:"required"`
	KategoriID      int32   `form:"kategori_id" json:"kategori_id" binding:"required"`
	KursusHarga     float64 `form:"kursus_harga" json:"kursus_harga"`
	KursusGambarURL string  `form:"kursus_gambar_url" json:"kursus_gambar_url"`
}

type UpdateKursusParameter struct {
	GetKursusParameter
	KursusNama      *string  `form:"kursus_nama" json:"kursus_nama"`
	KategoriID      *int32   `form:"kategori_id" json:"kategori_id"`
	KursusHarga     *float64 `form:"kursus_harga" json:"kursus_harga"`
	KursusGambarURL *string  `form:"kursus_gambar_url" json:"kursus_gambar_url"`
}
