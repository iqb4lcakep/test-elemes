package domaindata

import (
	"test_elemes/helpers"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Session struct is a row record of the session table in the test_elemes database
type Session struct {
	//[ 0] user_id                                        int                  null: false  primary: true   isArray: false  auto: false  col: int             len: -1      default: []
	UserID int32 `gorm:"primary_key;column:user_id;type:int;" json:"user_id"`
	//[ 1] session                                        text(65535)          null: false  primary: false  isArray: false  auto: false  col: text            len: 65535   default: []
	Session string `gorm:"column:session;type:text;size:65535;" json:"session"`
	//[ 2] session_status                                 int                  null: false  primary: false  isArray: false  auto: false  col: int             len: -1      default: []
	Status int32 `gorm:"column:status;type:int;" json:"status"`
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [current_timestamp()]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;" json:"created_at"`
	//[ 4] updated_at                                     timestamp            null: true   primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [NULL]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;" json:"updated_at"`
}

type PaginationParameter struct {
	Limit  int32 `form:"limit" json:"limit"`
	Offset int32 `form:"offset" json:"offset"`
}

type ListGlobalParameter struct {
	Search string `form:"search" json:"search"`
	OrderParameter
	PaginationParameter
}

type OrderParameter struct {
	OrderColumn *string `form:"order_column" json:"order_column"`
	OrderType   *string `form:"order_type" json:"order_type"`
}

type AuthJwtPayload struct {
	UserID int32 `json:"user_id"`
}

func (payload *AuthJwtPayload) CreateToken() (string, error) {
	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["created_at"] = time.Now().Unix()
	atClaims["user_id"] = payload.UserID

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(helpers.EnvVal("JWT_KEY", "test_elemes")))
	if err != nil {
		return "", err
	}
	return token, nil
}
