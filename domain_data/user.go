package domaindata

import (
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	//[ 0] user_id                                        int                  null: false  primary: true   isArray: false  auto: true   col: int             len: -1      default: []
	UserID int32 `gorm:"primary_key;AUTO_INCREMENT;column:user_id;type:int;" json:"user_id"`
	//[ 3] user_nama                                      varchar(100)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 100     default: []
	UserNama string `gorm:"column:user_nama;type:varchar;size:100;" json:"user_nama"`
	//[ 4] user_email                                     varchar(50)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 50      default: []
	UserEmail string `gorm:"column:user_email;type:varchar;size:50;" json:"user_email"`
	//[ 5] user_password                                  varchar(100)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 100     default: []
	UserPassword string `gorm:"column:user_password;type:varchar;size:100;" json:"user_password"`
	//[ 6] user_status                                    tinyint              null: false  primary: false  isArray: false  auto: false  col: tinyint         len: -1      default: [1]
	Status int32 `gorm:"column:status;type:tinyint;default:1;" json:"status"` // 0 = deactived\r\n1 = actived
	//[ 7] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [current_timestamp()]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;" json:"created_at"`
	//[ 8] updated_at                                     timestamp            null: true   primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [NULL]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;" json:"updated_at"`
	//[ 9] deleted_at                                     timestamp            null: true   primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [NULL]
	DeletedAt time.Time `gorm:"column:deleted_at;type:timestamp;" json:"deleted_at"`
}

type LoginParameter struct {
	UserEmail    string `form:"user_email" json:"user_email" binding:"required"`
	UserPassword string `form:"user_password" json:"user_password" binding:"required"`
}

func (user *User) VerifyPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(user.UserPassword), []byte(password))
	return err == nil
}

func (user *User) GenerateHashPassword(password string) (bool, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return false, err
	}
	user.UserPassword = string(hashedPassword)
	if user.VerifyPassword(password) {
		return true, nil
	}

	return false, errors.New("error generate hash passowrd")
}

type CreateUserParameter struct {
	UserNama string `form:"user_nama" json:"user_nama" binding:"required"`
	LoginParameter
}

type GetUserParameter struct {
	UserID int32 `form:"user_id" json:"user_id" binding:"required"`
}

type UpdateUserParameter struct {
	GetUserParameter
	UserNama     *string `form:"user_nama,omitempty" json:"user_nama"`
	UserEmail    *string `form:"user_email,omitempty" json:"user_email"`
	UserPassword *string `form:"user_password,omitempty" json:"user_password"`
}
