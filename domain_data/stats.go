package domaindata

type DashboardStats struct {
	TotalUser         int `json:"total_user"`
	TotalKursus       int `json:"total_kursus"`
	TotalKursusGratis int `json:"total_kursus_gratis"`
}
