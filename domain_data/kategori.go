package domaindata

type Kategori struct {
	KategoriID   int32  `gorm:"primary_key;AUTO_INCREMENT;column:kategori_id;type:int;" json:"kategori_id"`
	KategoriNama string `gorm:"column:kategori_nama;type:varchar;size:100;" json:"kategori_nama"`
}

type GetKategoriParameter struct {
	KategoriID int32 `form:"kategori_id" json:"kategori_id" binding:"required"`
}
