# TEST elemes

Nama : Muhammad Iqbal Rofikurrahman

## Kebutuhan dan Instalasi

- Golang
- MySQL server / Docker


## Usage

- buatlah `.env` file, lalu salin dan konfigurasikan seperti yang ada di `.env.example`

- impor postman dokumentasi

- jalankan perintah berikut

#### Docker
```bash
docker-compose up --build
```
#### Manual
- impor file `/mysql/test.sql` kedalam Mysql lokal server

```bash
go run main.go
```

#### Default Login
- user/pass : `test-elemes@gmail.com`/`elemes`