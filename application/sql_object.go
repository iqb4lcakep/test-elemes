package application

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

func Active(db *gorm.DB) *gorm.DB {
	return db.Where("status = ?", 1)
}

func Pagination(limit int32, offset int32) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if limit == 0 {
			limit = 10
		}
		return db.Offset(offset).Limit(limit)
	}
}

func Sort(column *string, order_type *string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if column != nil && order_type != nil {
			return db.Order(fmt.Sprintf("%s %s", *column, *order_type))
		}
		return db
	}
}
