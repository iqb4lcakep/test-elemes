package application

import "github.com/jinzhu/gorm"

type AppContext struct {
	Db *gorm.DB
}

func (ctx *AppContext) GetVerison() string {
	return "0.0.1"
}
