package application

import "github.com/gin-gonic/gin"

type ErrorKey string

const (
	ErrInput  ErrorKey = "INPUT_ERROR"
	ErrServer          = "SERVER_ERROR"
	ErrAuth            = "AUTH_ERROR"
)

func (app *AppContext) Success(c *gin.Context, msg string, data interface{}) {
	c.JSON(200, gin.H{
		"code":    200,
		"success": true,
		"msg":     msg,
		"data":    data,
	})
}

func (app *AppContext) Error(c *gin.Context, code int, msg string, key ErrorKey) {
	c.JSON(code, gin.H{
		"code":    code,
		"success": false,
		"msg":     msg,
		"key":     key,
	})
}
